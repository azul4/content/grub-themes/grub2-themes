# grub2-themes

grub2-themes: Stylish, Tela, Vimix, and Whitesur

https://github.com/vinceliuice/grub2-themes

<br>

How to clone this repository:
```
git clone https://gitlab.com/azul4/content/grub-themes/grub2-themes.git
```
<br><br>
## Available:

grub2-theme-vimix-1080p-color

grub2-theme-vimix-1080p-white

grub2-theme-vimix-2k-color

grub2-theme-vimix-2k-white

grub2-theme-vimix-4k-color

grub2-theme-vimix-4k-white

grub2-theme-tela-1080p-color

grub2-theme-tela-1080p-white

grub2-theme-tela-2k-color

grub2-theme-tela-2k-white

grub2-theme-tela-4k-color

grub2-theme-tela-4k-white

grub2-theme-stylish-1080p-color

grub2-theme-stylish-1080p-white

grub2-theme-stylish-2k-color

grub2-theme-stylish-2k-white

grub2-theme-stylish-4k-color

grub2-theme-stylish-4k-white

grub2-theme-whitesur-1080p-color

grub2-theme-whitesur-1080p-white

grub2-theme-whitesur-2k-color

grub2-theme-whitesur-2k-white

grub2-theme-whitesur-4k-color

grub2-theme-whitesur-4k-white

<br><br>

# Stylish:

<p align="center">
<img src="https://gitlab.com/azul4/images/grub2-themes/-/raw/main/background-stylish.jpg">
</p>

<br>

# Tela:

<p align="center">
<img src="https://gitlab.com/azul4/images/grub2-themes/-/raw/main/background-tela.jpg">
</p>

<br>

# Vimix:

<p align="center">
<img src="https://gitlab.com/azul4/images/grub2-themes/-/raw/main/background-vimix.jpg">
</p>

<br>

# Whitesur:

<p align="center">
<img src="https://gitlab.com/azul4/images/grub2-themes/-/raw/main/background-whitesur.jpg">
</p>


