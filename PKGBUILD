# Maintainer: Rafael <rcostarega@gmail.com> from azul repository
# With RebornOS icon addition

pkgbase=grub2-themes
pkgname=('grub2-theme-vimix-1080p-color'
         'grub2-theme-vimix-1080p-white'
         'grub2-theme-vimix-2k-color'
         'grub2-theme-vimix-2k-white'
         'grub2-theme-vimix-4k-color'
         'grub2-theme-vimix-4k-white'
         'grub2-theme-tela-1080p-color'
         'grub2-theme-tela-1080p-white'
         'grub2-theme-tela-2k-color'
         'grub2-theme-tela-2k-white'
         'grub2-theme-tela-4k-color'
         'grub2-theme-tela-4k-white'
         'grub2-theme-stylish-1080p-color'
         'grub2-theme-stylish-1080p-white'
         'grub2-theme-stylish-2k-color'
         'grub2-theme-stylish-2k-white'
         'grub2-theme-stylish-4k-color'
         'grub2-theme-stylish-4k-white'
         'grub2-theme-whitesur-1080p-color'
         'grub2-theme-whitesur-1080p-white'
         'grub2-theme-whitesur-2k-color'
         'grub2-theme-whitesur-2k-white'
         'grub2-theme-whitesur-4k-color'
         'grub2-theme-whitesur-4k-white')
fname1=grub2-theme-vimix
pkgver=2022.10.30
pkgrel=1
tname1=Vimix
tname2=Tela
tname3=Stylish
tname4=Whitesur
iname=rebornos
commitn=e77cbfa6625fbcc9782bb7779c82baf807fe1d72
pkgdesc="Grub2 themes from vinceliuice."
url='https://github.com/vinceliuice/grub2-themes'
arch=('any')
license=('GPLv3')
depends=(grub)
makedepends=(git)
install=${pkgbase}.install
source=("${fname1}::git+${url}#commit=${commitn}"
        "${iname}-color.png"
        "${iname}-white.png"
        "${pkgbase}.install"
        "unifont-regular-16.pf2")
conflicts=('grub2-theme-vimix')
sha256sums=('SKIP'
            '16dba477be79dc60a4d2538fee6fce09032ed812987e2599825c6facf44bd704'
            '934e8efa437857286ddc2d5cd5dfbd6c3b4e6cb5e473c7ce36c7d5b89ef9a06a'
            'bca552931b19538a09b1e8df4c8cd81fb3e9707ac4fe7f731ed87a64a2f45d87'
            '2c76695c6dbb3736a13a4b65b720aabe9595dce39cf5795aebbd74ba63901907')




package_grub2-theme-vimix-1080p-color() {
    pkgdesc="Grub2 theme Vimix 1080p, color icons. RebornOS icon added."
    
    cd ..
    install -dm755 ${pkgdir}/boot/grub/themes/${tname1}/icons
    install -Dm644 ${iname}-color.png ${pkgdir}/boot/grub/themes/${tname1}/icons/${iname}.png
    install -Dm644 unifont-regular-16.pf2 ${pkgdir}/boot/grub/themes/${tname1}
    
    cd ${srcdir}/${fname1}/common
    
    for file in *; do
        install -Dm644 ${file} ${pkgdir}/boot/grub/themes/${tname1}/${file}
    done

    cd ${srcdir}/${fname1}
    
    install -Dm644 backgrounds/1080p/background-vimix.jpg ${pkgdir}/boot/grub/themes/${tname1}/background.jpg
    install -dm644 assets/assets-color/icons ${pkgdir}/boot/grub/themes/${tname1}/icons
    
    cd ${srcdir}/${fname1}/assets/assets-color/icons-1080p
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname1}/icons/${file}
    done
    
    cd ${srcdir}/${fname1}/assets/assets-select/select-1080p
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname1}/${file}
    done
    
    install -Dm644 ${srcdir}/${fname1}/assets/info-1080p.png ${pkgdir}/boot/grub/themes/${tname1}/info.png
    install -Dm644 ${srcdir}/${fname1}/config/theme-1080p.txt ${pkgdir}/boot/grub/themes/${tname1}/theme.txt
}




package_grub2-theme-vimix-1080p-white() {
    pkgdesc="Grub2 theme Vimix 1080p, white icons. RebornOS icon added."
    
    cd ..
    install -dm755 ${pkgdir}/boot/grub/themes/${tname1}/icons
    install -Dm644 ${iname}-white.png ${pkgdir}/boot/grub/themes/${tname1}/icons/${iname}.png
    install -Dm644 unifont-regular-16.pf2 ${pkgdir}/boot/grub/themes/${tname1}
    
    cd ${srcdir}/${fname1}/common
    
    for file in *; do
        install -Dm644 ${file} ${pkgdir}/boot/grub/themes/${tname1}/${file}
    done

    cd ${srcdir}/${fname1}
    
    install -Dm644 backgrounds/1080p/background-vimix.jpg ${pkgdir}/boot/grub/themes/${tname1}/background.jpg
    install -dm644 assets/assets-white/icons ${pkgdir}/boot/grub/themes/${tname1}/icons
    
    cd ${srcdir}/${fname1}/assets/assets-white/icons-1080p
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname1}/icons/${file}
    done
    
    cd ${srcdir}/${fname1}/assets/assets-select/select-1080p
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname1}/${file}
    done
    
    install -Dm644 ${srcdir}/${fname1}/assets/info-1080p.png ${pkgdir}/boot/grub/themes/${tname1}/info.png
    install -Dm644 ${srcdir}/${fname1}/config/theme-1080p.txt ${pkgdir}/boot/grub/themes/${tname1}/theme.txt
}




package_grub2-theme-vimix-2k-color() {
pkgdesc="Grub2 theme Vimix 2k, color icons. RebornOS icon added."
    
    cd ..
    install -dm755 ${pkgdir}/boot/grub/themes/${tname1}/icons
    install -Dm644 ${iname}-color.png ${pkgdir}/boot/grub/themes/${tname1}/icons/${iname}.png
    install -Dm644 unifont-regular-16.pf2 ${pkgdir}/boot/grub/themes/${tname1}
    
    cd ${srcdir}/${fname1}/common
    
    for file in *; do
        install -Dm644 ${file} ${pkgdir}/boot/grub/themes/${tname1}/${file}
    done

    cd ${srcdir}/${fname1}
    
    install -Dm644 backgrounds/2k/background-vimix.jpg ${pkgdir}/boot/grub/themes/${tname1}/background.jpg
    install -dm644 assets/assets-color/icons ${pkgdir}/boot/grub/themes/${tname1}/icons
    
    cd ${srcdir}/${fname1}/assets/assets-color/icons-2k
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname1}/icons/${file}
    done
    
    cd ${srcdir}/${fname1}/assets/assets-select/select-2k
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname1}/${file}
    done
    
    install -Dm644 ${srcdir}/${fname1}/assets/info-2k.png ${pkgdir}/boot/grub/themes/${tname1}/info.png
    install -Dm644 ${srcdir}/${fname1}/config/theme-2k.txt ${pkgdir}/boot/grub/themes/${tname1}/theme.txt
}




package_grub2-theme-vimix-2k-white() {
pkgdesc="Grub2 theme Vimix 2k, white icons. RebornOS icon added."
    
    cd ..
    install -dm755 ${pkgdir}/boot/grub/themes/${tname1}/icons
    install -Dm644 ${iname}-white.png ${pkgdir}/boot/grub/themes/${tname1}/icons/${iname}.png
    install -Dm644 unifont-regular-16.pf2 ${pkgdir}/boot/grub/themes/${tname1}
    
    cd ${srcdir}/${fname1}/common
    
    for file in *; do
        install -Dm644 ${file} ${pkgdir}/boot/grub/themes/${tname1}/${file}
    done

    cd ${srcdir}/${fname1}
    
    install -Dm644 backgrounds/2k/background-vimix.jpg ${pkgdir}/boot/grub/themes/${tname1}/background.jpg
    install -dm644 assets/assets-white/icons ${pkgdir}/boot/grub/themes/${tname1}/icons
    
    cd ${srcdir}/${fname1}/assets/assets-white/icons-2k
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname1}/icons/${file}
    done
    
    cd ${srcdir}/${fname1}/assets/assets-select/select-2k
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname1}/${file}
    done
    
    install -Dm644 ${srcdir}/${fname1}/assets/info-2k.png ${pkgdir}/boot/grub/themes/${tname1}/info.png
    install -Dm644 ${srcdir}/${fname1}/config/theme-2k.txt ${pkgdir}/boot/grub/themes/${tname1}/theme.txt
}




package_grub2-theme-vimix-4k-color() {
pkgdesc="Grub2 theme Vimix 4k, color icons. RebornOS icon added."
    
    cd ..
    install -dm755 ${pkgdir}/boot/grub/themes/${tname1}/icons
    install -Dm644 ${iname}-color.png ${pkgdir}/boot/grub/themes/${tname1}/icons/${iname}.png
    install -Dm644 unifont-regular-16.pf2 ${pkgdir}/boot/grub/themes/${tname1}
    
    cd ${srcdir}/${fname1}/common
    
    for file in *; do
        install -Dm644 ${file} ${pkgdir}/boot/grub/themes/${tname1}/${file}
    done

    cd ${srcdir}/${fname1}
    
    install -Dm644 backgrounds/4k/background-vimix.jpg ${pkgdir}/boot/grub/themes/${tname1}/background.jpg
    install -dm644 assets/assets-color/icons ${pkgdir}/boot/grub/themes/${tname1}/icons
    
    cd ${srcdir}/${fname1}/assets/assets-color/icons-4k
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname1}/icons/${file}
    done
    
    cd ${srcdir}/${fname1}/assets/assets-select/select-4k
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname1}/${file}
    done
    
    install -Dm644 ${srcdir}/${fname1}/assets/info-4k.png ${pkgdir}/boot/grub/themes/${tname1}/info.png
    install -Dm644 ${srcdir}/${fname1}/config/theme-4k.txt ${pkgdir}/boot/grub/themes/${tname1}/theme.txt
}




package_grub2-theme-vimix-4k-white() {
pkgdesc="Grub2 theme Vimix 4k, white icons. RebornOS icon added."
    
    cd ..
    install -dm755 ${pkgdir}/boot/grub/themes/${tname1}/icons
    install -Dm644 ${iname}-white.png ${pkgdir}/boot/grub/themes/${tname1}/icons/${iname}.png
    install -Dm644 unifont-regular-16.pf2 ${pkgdir}/boot/grub/themes/${tname1}
    
    cd ${srcdir}/${fname1}/common
    
    for file in *; do
        install -Dm644 ${file} ${pkgdir}/boot/grub/themes/${tname1}/${file}
    done

    cd ${srcdir}/${fname1}
    
    install -Dm644 backgrounds/4k/background-vimix.jpg ${pkgdir}/boot/grub/themes/${tname1}/background.jpg
    install -dm644 assets/assets-white/icons ${pkgdir}/boot/grub/themes/${tname1}/icons
    
    cd ${srcdir}/${fname1}/assets/assets-white/icons-4k
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname1}/icons/${file}
    done
    
    cd ${srcdir}/${fname1}/assets/assets-select/select-4k
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname1}/${file}
    done
    
    install -Dm644 ${srcdir}/${fname1}/assets/info-4k.png ${pkgdir}/boot/grub/themes/${tname1}/info.png
    install -Dm644 ${srcdir}/${fname1}/config/theme-4k.txt ${pkgdir}/boot/grub/themes/${tname1}/theme.txt
}




package_grub2-theme-tela-1080p-color() {
    pkgdesc="Grub2 theme Tela 1080p, color icons. RebornOS icon added."
    
    cd ..
    install -dm755 ${pkgdir}/boot/grub/themes/${tname2}/icons
    install -Dm644 ${iname}-color.png ${pkgdir}/boot/grub/themes/${tname2}/icons/${iname}.png
    install -Dm644 unifont-regular-16.pf2 ${pkgdir}/boot/grub/themes/${tname2}
    
    cd ${srcdir}/${fname1}/common
    
    for file in *; do
        install -Dm644 ${file} ${pkgdir}/boot/grub/themes/${tname2}/${file}
    done

    cd ${srcdir}/${fname1}
    
    install -Dm644 backgrounds/1080p/background-tela.jpg ${pkgdir}/boot/grub/themes/${tname2}/background.jpg
    install -dm644 assets/assets-color/icons ${pkgdir}/boot/grub/themes/${tname2}/icons
    
    cd ${srcdir}/${fname1}/assets/assets-color/icons-1080p
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname2}/icons/${file}
    done
    
    cd ${srcdir}/${fname1}/assets/assets-select/select-1080p
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname2}/${file}
    done
    
    install -Dm644 ${srcdir}/${fname1}/assets/info-1080p.png ${pkgdir}/boot/grub/themes/${tname2}/info.png
    install -Dm644 ${srcdir}/${fname1}/config/theme-1080p.txt ${pkgdir}/boot/grub/themes/${tname2}/theme.txt
}




package_grub2-theme-tela-1080p-white() {
    pkgdesc="Grub2 theme Tela 1080p, white icons. RebornOS icon added."
    
    cd ..
    install -dm755 ${pkgdir}/boot/grub/themes/${tname2}/icons
    install -Dm644 ${iname}-white.png ${pkgdir}/boot/grub/themes/${tname2}/icons/${iname}.png
    install -Dm644 unifont-regular-16.pf2 ${pkgdir}/boot/grub/themes/${tname2}
    
    cd ${srcdir}/${fname1}/common
    
    for file in *; do
        install -Dm644 ${file} ${pkgdir}/boot/grub/themes/${tname2}/${file}
    done

    cd ${srcdir}/${fname1}
    
    install -Dm644 backgrounds/1080p/background-tela.jpg ${pkgdir}/boot/grub/themes/${tname2}/background.jpg
    install -dm644 assets/assets-white/icons ${pkgdir}/boot/grub/themes/${tname2}/icons
    
    cd ${srcdir}/${fname1}/assets/assets-white/icons-1080p
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname2}/icons/${file}
    done
    
    cd ${srcdir}/${fname1}/assets/assets-select/select-1080p
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname2}/${file}
    done
    
    install -Dm644 ${srcdir}/${fname1}/assets/info-1080p.png ${pkgdir}/boot/grub/themes/${tname2}/info.png
    install -Dm644 ${srcdir}/${fname1}/config/theme-1080p.txt ${pkgdir}/boot/grub/themes/${tname2}/theme.txt
}




package_grub2-theme-tela-2k-color() {
pkgdesc="Grub2 theme Tela 2k, color icons. RebornOS icon added."
    
    cd ..
    install -dm755 ${pkgdir}/boot/grub/themes/${tname2}/icons
    install -Dm644 ${iname}-color.png ${pkgdir}/boot/grub/themes/${tname2}/icons/${iname}.png
    install -Dm644 unifont-regular-16.pf2 ${pkgdir}/boot/grub/themes/${tname2}
    
    cd ${srcdir}/${fname1}/common
    
    for file in *; do
        install -Dm644 ${file} ${pkgdir}/boot/grub/themes/${tname2}/${file}
    done

    cd ${srcdir}/${fname1}
    
    install -Dm644 backgrounds/2k/background-tela.jpg ${pkgdir}/boot/grub/themes/${tname2}/background.jpg
    install -dm644 assets/assets-color/icons ${pkgdir}/boot/grub/themes/${tname2}/icons
    
    cd ${srcdir}/${fname1}/assets/assets-color/icons-2k
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname2}/icons/${file}
    done
    
    cd ${srcdir}/${fname1}/assets/assets-select/select-2k
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname2}/${file}
    done
    
    install -Dm644 ${srcdir}/${fname1}/assets/info-2k.png ${pkgdir}/boot/grub/themes/${tname2}/info.png
    install -Dm644 ${srcdir}/${fname1}/config/theme-2k.txt ${pkgdir}/boot/grub/themes/${tname2}/theme.txt
}




package_grub2-theme-tela-2k-white() {
pkgdesc="Grub2 theme Tela 2k, white icons. RebornOS icon added."
    
    cd ..
    install -dm755 ${pkgdir}/boot/grub/themes/${tname2}/icons
    install -Dm644 ${iname}-white.png ${pkgdir}/boot/grub/themes/${tname2}/icons/${iname}.png
    install -Dm644 unifont-regular-16.pf2 ${pkgdir}/boot/grub/themes/${tname2}
    
    cd ${srcdir}/${fname1}/common
    
    for file in *; do
        install -Dm644 ${file} ${pkgdir}/boot/grub/themes/${tname2}/${file}
    done

    cd ${srcdir}/${fname1}
    
    install -Dm644 backgrounds/2k/background-tela.jpg ${pkgdir}/boot/grub/themes/${tname2}/background.jpg
    install -dm644 assets/assets-white/icons ${pkgdir}/boot/grub/themes/${tname2}/icons
    
    cd ${srcdir}/${fname1}/assets/assets-white/icons-2k
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname2}/icons/${file}
    done
    
    cd ${srcdir}/${fname1}/assets/assets-select/select-2k
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname2}/${file}
    done
    
    install -Dm644 ${srcdir}/${fname1}/assets/info-2k.png ${pkgdir}/boot/grub/themes/${tname2}/info.png
    install -Dm644 ${srcdir}/${fname1}/config/theme-2k.txt ${pkgdir}/boot/grub/themes/${tname2}/theme.txt
}




package_grub2-theme-tela-4k-color() {
pkgdesc="Grub2 theme Tela 4k, color icons. RebornOS icon added."
    
    cd ..
    install -dm755 ${pkgdir}/boot/grub/themes/${tname2}/icons
    install -Dm644 ${iname}-color.png ${pkgdir}/boot/grub/themes/${tname2}/icons/${iname}.png
    install -Dm644 unifont-regular-16.pf2 ${pkgdir}/boot/grub/themes/${tname2}
    
    cd ${srcdir}/${fname1}/common
    
    for file in *; do
        install -Dm644 ${file} ${pkgdir}/boot/grub/themes/${tname2}/${file}
    done

    cd ${srcdir}/${fname1}
    
    install -Dm644 backgrounds/4k/background-tela.jpg ${pkgdir}/boot/grub/themes/${tname2}/background.jpg
    install -dm644 assets/assets-color/icons ${pkgdir}/boot/grub/themes/${tname2}/icons
    
    cd ${srcdir}/${fname1}/assets/assets-color/icons-4k
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname2}/icons/${file}
    done
    
    cd ${srcdir}/${fname1}/assets/assets-select/select-4k
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname2}/${file}
    done
    
    install -Dm644 ${srcdir}/${fname1}/assets/info-4k.png ${pkgdir}/boot/grub/themes/${tname2}/info.png
    install -Dm644 ${srcdir}/${fname1}/config/theme-4k.txt ${pkgdir}/boot/grub/themes/${tname2}/theme.txt
}




package_grub2-theme-tela-4k-white() {
pkgdesc="Grub2 theme Tela 4k, white icons. RebornOS icon added."
    
    cd ..
    install -dm755 ${pkgdir}/boot/grub/themes/${tname2}/icons
    install -Dm644 ${iname}-white.png ${pkgdir}/boot/grub/themes/${tname2}/icons/${iname}.png
    install -Dm644 unifont-regular-16.pf2 ${pkgdir}/boot/grub/themes/${tname2}
    
    cd ${srcdir}/${fname1}/common
    
    for file in *; do
        install -Dm644 ${file} ${pkgdir}/boot/grub/themes/${tname2}/${file}
    done

    cd ${srcdir}/${fname1}
    
    install -Dm644 backgrounds/4k/background-tela.jpg ${pkgdir}/boot/grub/themes/${tname2}/background.jpg
    install -dm644 assets/assets-white/icons ${pkgdir}/boot/grub/themes/${tname2}/icons
    
    cd ${srcdir}/${fname1}/assets/assets-white/icons-4k
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname2}/icons/${file}
    done
    
    cd ${srcdir}/${fname1}/assets/assets-select/select-4k
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname2}/${file}
    done
    
    install -Dm644 ${srcdir}/${fname1}/assets/info-4k.png ${pkgdir}/boot/grub/themes/${tname2}/info.png
    install -Dm644 ${srcdir}/${fname1}/config/theme-4k.txt ${pkgdir}/boot/grub/themes/${tname2}/theme.txt
}



package_grub2-theme-stylish-1080p-color() {
    pkgdesc="Grub2 theme Stylish 1080p, color icons. RebornOS icon added."
    
    cd ..
    install -dm755 ${pkgdir}/boot/grub/themes/${tname3}/icons
    install -Dm644 ${iname}-color.png ${pkgdir}/boot/grub/themes/${tname3}/icons/${iname}.png
    install -Dm644 unifont-regular-16.pf2 ${pkgdir}/boot/grub/themes/${tname3}
    
    cd ${srcdir}/${fname1}/common
    
    for file in *; do
        install -Dm644 ${file} ${pkgdir}/boot/grub/themes/${tname3}/${file}
    done

    cd ${srcdir}/${fname1}
    
    install -Dm644 backgrounds/1080p/background-stylish.jpg ${pkgdir}/boot/grub/themes/${tname3}/background.jpg
    install -dm644 assets/assets-color/icons ${pkgdir}/boot/grub/themes/${tname3}/icons
    
    cd ${srcdir}/${fname1}/assets/assets-color/icons-1080p
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname3}/icons/${file}
    done
    
    cd ${srcdir}/${fname1}/assets/assets-select/select-1080p
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname3}/${file}
    done
    
    install -Dm644 ${srcdir}/${fname1}/assets/info-1080p.png ${pkgdir}/boot/grub/themes/${tname3}/info.png
    install -Dm644 ${srcdir}/${fname1}/config/theme-1080p.txt ${pkgdir}/boot/grub/themes/${tname3}/theme.txt
}




package_grub2-theme-stylish-1080p-white() {
    pkgdesc="Grub2 theme Stylish 1080p, white icons. RebornOS icon added."
    
    cd ..
    install -dm755 ${pkgdir}/boot/grub/themes/${tname3}/icons
    install -Dm644 ${iname}-white.png ${pkgdir}/boot/grub/themes/${tname3}/icons/${iname}.png
    install -Dm644 unifont-regular-16.pf2 ${pkgdir}/boot/grub/themes/${tname3}
    
    cd ${srcdir}/${fname1}/common
    
    for file in *; do
        install -Dm644 ${file} ${pkgdir}/boot/grub/themes/${tname3}/${file}
    done

    cd ${srcdir}/${fname1}
    
    install -Dm644 backgrounds/1080p/background-stylish.jpg ${pkgdir}/boot/grub/themes/${tname3}/background.jpg
    install -dm644 assets/assets-white/icons ${pkgdir}/boot/grub/themes/${tname3}/icons
    
    cd ${srcdir}/${fname1}/assets/assets-white/icons-1080p
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname3}/icons/${file}
    done
    
    cd ${srcdir}/${fname1}/assets/assets-select/select-1080p
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname3}/${file}
    done
    
    install -Dm644 ${srcdir}/${fname1}/assets/info-1080p.png ${pkgdir}/boot/grub/themes/${tname3}/info.png
    install -Dm644 ${srcdir}/${fname1}/config/theme-1080p.txt ${pkgdir}/boot/grub/themes/${tname3}/theme.txt
}



package_grub2-theme-stylish-2k-color() {
pkgdesc="Grub2 theme Stylish 2k, color icons. RebornOS icon added."
    
    cd ..
    install -dm755 ${pkgdir}/boot/grub/themes/${tname3}/icons
    install -Dm644 ${iname}-color.png ${pkgdir}/boot/grub/themes/${tname3}/icons/${iname}.png
    install -Dm644 unifont-regular-16.pf2 ${pkgdir}/boot/grub/themes/${tname3}
    
    cd ${srcdir}/${fname1}/common
    
    for file in *; do
        install -Dm644 ${file} ${pkgdir}/boot/grub/themes/${tname3}/${file}
    done

    cd ${srcdir}/${fname1}
    
    install -Dm644 backgrounds/2k/background-stylish.jpg ${pkgdir}/boot/grub/themes/${tname3}/background.jpg
    install -dm644 assets/assets-color/icons ${pkgdir}/boot/grub/themes/${tname3}/icons
    
    cd ${srcdir}/${fname1}/assets/assets-color/icons-2k
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname3}/icons/${file}
    done
    
    cd ${srcdir}/${fname1}/assets/assets-select/select-2k
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname3}/${file}
    done
    
    install -Dm644 ${srcdir}/${fname1}/assets/info-2k.png ${pkgdir}/boot/grub/themes/${tname3}/info.png
    install -Dm644 ${srcdir}/${fname1}/config/theme-2k.txt ${pkgdir}/boot/grub/themes/${tname3}/theme.txt
}




package_grub2-theme-stylish-2k-white() {
pkgdesc="Grub2 theme Stylish 2k, white icons. RebornOS icon added."
    
    cd ..
    install -dm755 ${pkgdir}/boot/grub/themes/${tname3}/icons
    install -Dm644 ${iname}-white.png ${pkgdir}/boot/grub/themes/${tname3}/icons/${iname}.png
    install -Dm644 unifont-regular-16.pf2 ${pkgdir}/boot/grub/themes/${tname3}
    
    cd ${srcdir}/${fname1}/common
    
    for file in *; do
        install -Dm644 ${file} ${pkgdir}/boot/grub/themes/${tname3}/${file}
    done

    cd ${srcdir}/${fname1}
    
    install -Dm644 backgrounds/2k/background-stylish.jpg ${pkgdir}/boot/grub/themes/${tname3}/background.jpg
    install -dm644 assets/assets-white/icons ${pkgdir}/boot/grub/themes/${tname3}/icons
    
    cd ${srcdir}/${fname1}/assets/assets-white/icons-2k
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname3}/icons/${file}
    done
    
    cd ${srcdir}/${fname1}/assets/assets-select/select-2k
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname3}/${file}
    done
    
    install -Dm644 ${srcdir}/${fname1}/assets/info-2k.png ${pkgdir}/boot/grub/themes/${tname3}/info.png
    install -Dm644 ${srcdir}/${fname1}/config/theme-2k.txt ${pkgdir}/boot/grub/themes/${tname3}/theme.txt
}



package_grub2-theme-stylish-4k-color() {
pkgdesc="Grub2 theme Stylish 4k, color icons. RebornOS icon added."
    
    cd ..
    install -dm755 ${pkgdir}/boot/grub/themes/${tname3}/icons
    install -Dm644 ${iname}-color.png ${pkgdir}/boot/grub/themes/${tname3}/icons/${iname}.png
    install -Dm644 unifont-regular-16.pf2 ${pkgdir}/boot/grub/themes/${tname3}
    
    cd ${srcdir}/${fname1}/common
    
    for file in *; do
        install -Dm644 ${file} ${pkgdir}/boot/grub/themes/${tname3}/${file}
    done

    cd ${srcdir}/${fname1}
    
    install -Dm644 backgrounds/4k/background-stylish.jpg ${pkgdir}/boot/grub/themes/${tname3}/background.jpg
    install -dm644 assets/assets-color/icons ${pkgdir}/boot/grub/themes/${tname3}/icons
    
    cd ${srcdir}/${fname1}/assets/assets-color/icons-4k
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname3}/icons/${file}
    done
    
    cd ${srcdir}/${fname1}/assets/assets-select/select-4k
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname3}/${file}
    done
    
    install -Dm644 ${srcdir}/${fname1}/assets/info-4k.png ${pkgdir}/boot/grub/themes/${tname3}/info.png
    install -Dm644 ${srcdir}/${fname1}/config/theme-4k.txt ${pkgdir}/boot/grub/themes/${tname3}/theme.txt
}




package_grub2-theme-stylish-4k-white() {
pkgdesc="Grub2 theme Stylish 4k, white icons. RebornOS icon added."
    
    cd ..
    install -dm755 ${pkgdir}/boot/grub/themes/${tname3}/icons
    install -Dm644 ${iname}-white.png ${pkgdir}/boot/grub/themes/${tname3}/icons/${iname}.png
    install -Dm644 unifont-regular-16.pf2 ${pkgdir}/boot/grub/themes/${tname3}
    
    cd ${srcdir}/${fname1}/common
    
    for file in *; do
        install -Dm644 ${file} ${pkgdir}/boot/grub/themes/${tname3}/${file}
    done

    cd ${srcdir}/${fname1}
    
    install -Dm644 backgrounds/4k/background-stylish.jpg ${pkgdir}/boot/grub/themes/${tname3}/background.jpg
    install -dm644 assets/assets-white/icons ${pkgdir}/boot/grub/themes/${tname3}/icons
    
    cd ${srcdir}/${fname1}/assets/assets-white/icons-4k
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname3}/icons/${file}
    done
    
    cd ${srcdir}/${fname1}/assets/assets-select/select-4k
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname3}/${file}
    done
    
    install -Dm644 ${srcdir}/${fname1}/assets/info-4k.png ${pkgdir}/boot/grub/themes/${tname3}/info.png
    install -Dm644 ${srcdir}/${fname1}/config/theme-4k.txt ${pkgdir}/boot/grub/themes/${tname3}/theme.txt
}




package_grub2-theme-whitesur-1080p-color() {
    pkgdesc="Grub2 theme Whitesur 1080p, color icons. RebornOS icon added."
    
    cd ..
    install -dm755 ${pkgdir}/boot/grub/themes/${tname4}/icons
    install -Dm644 ${iname}-color.png ${pkgdir}/boot/grub/themes/${tname4}/icons/${iname}.png
    install -Dm644 unifont-regular-16.pf2 ${pkgdir}/boot/grub/themes/${tname4}
    
    cd ${srcdir}/${fname1}/common
    
    for file in *; do
        install -Dm644 ${file} ${pkgdir}/boot/grub/themes/${tname4}/${file}
    done

    cd ${srcdir}/${fname1}
    
    install -Dm644 backgrounds/1080p/background-whitesur.jpg ${pkgdir}/boot/grub/themes/${tname4}/background.jpg
    install -dm644 assets/assets-color/icons ${pkgdir}/boot/grub/themes/${tname4}/icons
    
    cd ${srcdir}/${fname1}/assets/assets-color/icons-1080p
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname4}/icons/${file}
    done
    
    cd ${srcdir}/${fname1}/assets/assets-select/select-1080p
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname4}/${file}
    done
    
    install -Dm644 ${srcdir}/${fname1}/assets/info-1080p.png ${pkgdir}/boot/grub/themes/${tname4}/info.png
    install -Dm644 ${srcdir}/${fname1}/config/theme-1080p.txt ${pkgdir}/boot/grub/themes/${tname4}/theme.txt
}




package_grub2-theme-whitesur-1080p-white() {
    pkgdesc="Grub2 theme Whitesur 1080p, white icons. RebornOS icon added."
    
    cd ..
    install -dm755 ${pkgdir}/boot/grub/themes/${tname4}/icons
    install -Dm644 ${iname}-white.png ${pkgdir}/boot/grub/themes/${tname4}/icons/${iname}.png
    install -Dm644 unifont-regular-16.pf2 ${pkgdir}/boot/grub/themes/${tname4}
    
    cd ${srcdir}/${fname1}/common
    
    for file in *; do
        install -Dm644 ${file} ${pkgdir}/boot/grub/themes/${tname4}/${file}
    done

    cd ${srcdir}/${fname1}
    
    install -Dm644 backgrounds/1080p/background-whitesur.jpg ${pkgdir}/boot/grub/themes/${tname4}/background.jpg
    install -dm644 assets/assets-white/icons ${pkgdir}/boot/grub/themes/${tname4}/icons
    
    cd ${srcdir}/${fname1}/assets/assets-white/icons-1080p
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname4}/icons/${file}
    done
    
    cd ${srcdir}/${fname1}/assets/assets-select/select-1080p
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname4}/${file}
    done
    
    install -Dm644 ${srcdir}/${fname1}/assets/info-1080p.png ${pkgdir}/boot/grub/themes/${tname4}/info.png
    install -Dm644 ${srcdir}/${fname1}/config/theme-1080p.txt ${pkgdir}/boot/grub/themes/${tname4}/theme.txt
}



package_grub2-theme-whitesur-2k-color() {
pkgdesc="Grub2 theme Whitesur 2k, color icons. RebornOS icon added."
    
    cd ..
    install -dm755 ${pkgdir}/boot/grub/themes/${tname4}/icons
    install -Dm644 ${iname}-color.png ${pkgdir}/boot/grub/themes/${tname4}/icons/${iname}.png
    install -Dm644 unifont-regular-16.pf2 ${pkgdir}/boot/grub/themes/${tname4}
    
    cd ${srcdir}/${fname1}/common
    
    for file in *; do
        install -Dm644 ${file} ${pkgdir}/boot/grub/themes/${tname4}/${file}
    done

    cd ${srcdir}/${fname1}
    
    install -Dm644 backgrounds/2k/background-whitesur.jpg ${pkgdir}/boot/grub/themes/${tname4}/background.jpg
    install -dm644 assets/assets-color/icons ${pkgdir}/boot/grub/themes/${tname4}/icons
    
    cd ${srcdir}/${fname1}/assets/assets-color/icons-2k
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname4}/icons/${file}
    done
    
    cd ${srcdir}/${fname1}/assets/assets-select/select-2k
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname4}/${file}
    done
    
    install -Dm644 ${srcdir}/${fname1}/assets/info-2k.png ${pkgdir}/boot/grub/themes/${tname4}/info.png
    install -Dm644 ${srcdir}/${fname1}/config/theme-2k.txt ${pkgdir}/boot/grub/themes/${tname4}/theme.txt
}




package_grub2-theme-whitesur-2k-white() {
pkgdesc="Grub2 theme Whitesur 2k, white icons. RebornOS icon added."
    
    cd ..
    install -dm755 ${pkgdir}/boot/grub/themes/${tname4}/icons
    install -Dm644 ${iname}-white.png ${pkgdir}/boot/grub/themes/${tname4}/icons/${iname}.png
    install -Dm644 unifont-regular-16.pf2 ${pkgdir}/boot/grub/themes/${tname4}
    
    cd ${srcdir}/${fname1}/common
    
    for file in *; do
        install -Dm644 ${file} ${pkgdir}/boot/grub/themes/${tname4}/${file}
    done

    cd ${srcdir}/${fname1}
    
    install -Dm644 backgrounds/2k/background-whitesur.jpg ${pkgdir}/boot/grub/themes/${tname4}/background.jpg
    install -dm644 assets/assets-white/icons ${pkgdir}/boot/grub/themes/${tname4}/icons
    
    cd ${srcdir}/${fname1}/assets/assets-white/icons-2k
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname4}/icons/${file}
    done
    
    cd ${srcdir}/${fname1}/assets/assets-select/select-2k
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname4}/${file}
    done
    
    install -Dm644 ${srcdir}/${fname1}/assets/info-2k.png ${pkgdir}/boot/grub/themes/${tname4}/info.png
    install -Dm644 ${srcdir}/${fname1}/config/theme-2k.txt ${pkgdir}/boot/grub/themes/${tname4}/theme.txt
}



package_grub2-theme-whitesur-4k-color() {
pkgdesc="Grub2 theme Whitesur 4k, color icons. RebornOS icon added."
    
    cd ..
    install -dm755 ${pkgdir}/boot/grub/themes/${tname4}/icons
    install -Dm644 ${iname}-color.png ${pkgdir}/boot/grub/themes/${tname4}/icons/${iname}.png
    install -Dm644 unifont-regular-16.pf2 ${pkgdir}/boot/grub/themes/${tname4}
    
    cd ${srcdir}/${fname1}/common
    
    for file in *; do
        install -Dm644 ${file} ${pkgdir}/boot/grub/themes/${tname4}/${file}
    done

    cd ${srcdir}/${fname1}
    
    install -Dm644 backgrounds/4k/background-whitesur.jpg ${pkgdir}/boot/grub/themes/${tname4}/background.jpg
    install -dm644 assets/assets-color/icons ${pkgdir}/boot/grub/themes/${tname4}/icons
    
    cd ${srcdir}/${fname1}/assets/assets-color/icons-4k
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname4}/icons/${file}
    done
    
    cd ${srcdir}/${fname1}/assets/assets-select/select-4k
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname4}/${file}
    done
    
    install -Dm644 ${srcdir}/${fname1}/assets/info-4k.png ${pkgdir}/boot/grub/themes/${tname4}/info.png
    install -Dm644 ${srcdir}/${fname1}/config/theme-4k.txt ${pkgdir}/boot/grub/themes/${tname4}/theme.txt
}




package_grub2-theme-whitesur-4k-white() {
pkgdesc="Grub2 theme Whitesur 4k, white icons. RebornOS icon added."
    
    cd ..
    install -dm755 ${pkgdir}/boot/grub/themes/${tname4}/icons
    install -Dm644 ${iname}-white.png ${pkgdir}/boot/grub/themes/${tname4}/icons/${iname}.png
    install -Dm644 unifont-regular-16.pf2 ${pkgdir}/boot/grub/themes/${tname4}
    
    cd ${srcdir}/${fname1}/common
    
    for file in *; do
        install -Dm644 ${file} ${pkgdir}/boot/grub/themes/${tname4}/${file}
    done

    cd ${srcdir}/${fname1}
    
    install -Dm644 backgrounds/4k/background-whitesur.jpg ${pkgdir}/boot/grub/themes/${tname4}/background.jpg
    install -dm644 assets/assets-white/icons ${pkgdir}/boot/grub/themes/${tname4}/icons
    
    cd ${srcdir}/${fname1}/assets/assets-white/icons-4k
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname4}/icons/${file}
    done
    
    cd ${srcdir}/${fname1}/assets/assets-select/select-4k
    
    for file in *.png; do
        install -m644 ${file} ${pkgdir}/boot/grub/themes/${tname4}/${file}
    done
    
    install -Dm644 ${srcdir}/${fname1}/assets/info-4k.png ${pkgdir}/boot/grub/themes/${tname4}/info.png
    install -Dm644 ${srcdir}/${fname1}/config/theme-4k.txt ${pkgdir}/boot/grub/themes/${tname4}/theme.txt
}

